//
//  ViewController.swift
//  Facebook_Integration
//
//  Created by Amol Tamboli on 23/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import FacebookLogin
import FBSDKLoginKit

class ViewController: UIViewController {

    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var fbLoginBtn: UIButton!
    @IBOutlet weak var btnLogOut: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnLogOut.isHidden = true
    }

    @IBAction func logOut(_ sender: Any) {
        let logOut = LoginManager()
        logOut.logOut()
        fbLoginBtn.isHidden = false
    }
    @IBAction func facebookActionBtn(_ sender: Any) {
        self.facebookLogin()
        getFacebookData()
    }
    
    //MARK:- Facebook Login Function
    func facebookLogin(){
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { (result) in
            switch result {
                
            case .success(_,_,_):
                self.getFacebookData()
            case .cancelled:
                print("User Clicked On Cancelled button")
            case .failed(let error):
                print(error.localizedDescription)
            }
        }
    }
    
    func getFacebookData(){
        if AccessToken.current != nil {
            GraphRequest(graphPath: "me", parameters: ["Fields":"id, email, name, picture.type(large)"]).start { (connection, result, error) in
                if error == nil {
                    let dict = result as! [String:AnyObject] as NSDictionary
                    
                    let name = dict.object(forKey: "name") as! String
                    let email = dict.object(forKey: "email") as! String
                    let id = dict.object(forKey: "id") as! String
                    
                    print("Name:\(name)")
                    print("Email:\(email)")
                    print("Id:\(id)")
                    print(dict)
                    
                    self.lblName.text = "Name:- " + name
                    self.lblEmail.text = "Email:- " + email
                    self.lblId.text = "Id:- " + id

                    //MARK:- 71-82 Profile picture code
                    if let profilePicture = dict.object(forKey: "picture") as? [String:Any]{
                        if let profilePicData = profilePicture["data"] as? [String:Any]{
                            print("\(profilePicData)")
                            if let profilepic = profilePicData["url"] as? URL{
                                print(profilepic)
                                let data = try? Data(contentsOf: profilepic)
                                if let imageData = data {
                                    self.img.image = UIImage(data: imageData)
                                }
                            }
                        }
                    }
                    self.fbLoginBtn.isHidden = true
                    self.btnLogOut.isHidden = false
                } else {
                    print(error?.localizedDescription)
                }
            }
        } else {
            print("Access Token is NIL")
        }
    }
}

